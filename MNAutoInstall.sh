#!/bin/bash

clear
# màu mè
DF_BG_COLOR="\e[44m"
DF_TEXT_COLOR="\e[1m"
RESET="\e[0m"
CYAN="\e[91m"
GREEN="\e[92m"
YELLOW="\e[95m"
RED="\e[92m"
PINK="\e[94m"

export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "{$YELLOW}"
printf "\n###############################\n"
printf "## MASTERNODE AUTO-INSTALLER ##\n"
printf "###############################\n\n"
printf "{$RESET}"
sleep 3
##########################
## KHAI BÁO BIẾN CỤC BỘ ##
##########################
COIN_NAME="citrus"
WALLET_DIR=".citruscore"
PORT_NUM="11112"
RPC_PORT_NUM="3132"
GIT_REPO_URL="https://github.com/citruscoin/Citrus.git"
REPO_DIR="Citrus" # THƯ MỤC KHI PULL GIT VỀ
HOME_DIR=$( getent passwd "$USER" | cut -d: -f6 )
CONF_DIR="$HOME_DIR/$WALLET_DIR"
CONF_FILE="$CONF_DIR/$COIN_NAME.conf"
LOCAL_BIN="/usr/local/bin/"

DOES_COIN_USE_SEPERATE_CLI="y"

DAEMON_SUFFIX="d"
DAEMON_NAME=$COIN_NAME$DAEMON_SUFFIX
DAEMON_PATH=$LOCAL_BIN$DAEMON_NAME
CLI_SUFFIX="-cli"

read -p "{$GREEN}Would you like to configure SWAP (Recommend if your RAM below 2GB)? {$RED}(y/n)" -n 1 -r
echo ""
SWAP_FILE=$REPLY

read -p "{$GREEN}Would you like to setup the UFW firewall? (Recommend) {$RED}(y/n)" -n 1 -r
echo ""
SETUP_FW=$REPLY

read -p "{$GREEN}Would you like to install Fail2Ban? (Recommend) {$RED}(y/n)?" -n 1 -r
echo ""
FAIL_TO_BAN=$REPLY

set -e
sudo apt-get install pwgen jq --assume-yes
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> GETTING YOUR EXTERNAL IP...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n 		{$CYAN}==>GETTING YOUR EXTERNAL IP...{$RESET} \n"
EXTERNAL_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)
RPC_USER=$(pwgen 50 1)
RPC_PASS=$(pwgen 50 1)

if [ "$DOES_COIN_USE_SEPERATE_CLI" = "y" ]
then
	CLI_NAME=$COIN_NAME$CLI_SUFFIX
	CLI_PATH=$LOCAL_BIN$CLI_NAME
else
	echo -e "\e[31mCLI path set to daemon path\e[0m"

	CLI_NAME=$DAEMON_NAME
	CLI_PATH=$DAEMON_PATH
fi

##################
## CÀI ĐẶT SWAP ##
##################
if [ $SWAP_FILE = "y" ]
	export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> CONFIGURING SWAP...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
	printf "\n\n 		{$CYAN}==>CONFIGURING SWAP...{$RESET} \n\n"
	then
	sudo dd if=/dev/zero of=/swapfile count=2048 bs=1M
	sudo chmod 600 /swapfile
	sudo mkswap /swapfile
	sudo swapon /swapfile
	printf "/swapfile   none    swap    sw    0   0" >> /etc/fstab
fi

if [ $FAIL_TO_BAN = "y" ]
then
	sudo apt-get -y install fail2ban
	sudo systemctl enable fail2ban
	sudo systemctl start fail2ban
fi

##################
## CÀI ĐẶT VÍ   ##
##################
## Cài đặt các pkg cần thiết

export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> UPDATING SYSTEM AND INSTALLING NECESSARY PACKAGES...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>UPDATING SYSTEM AND INSTALLING NECESSARY PACKAGES...{$RESET} \n\n"
sudo add-apt-repository ppa:bitcoin/bitcoin -y
sudo apt-get update
sudo apt-get upgrade -qq -y
sudo apt-get dist-upgrade -qq -y
sudo apt-get install nano htop -y
sudo apt-get install build-essential libtool autotools-dev automake pkg-config -y
sudo apt-get install libssl-dev libevent-dev bsdmainutils software-properties-common python-software-properties -y
sudo apt-get install libboost-all-dev -y
sudo apt-get install libzmq3-dev libminiupnpc-dev -y
sudo apt-get install libdb4.8-dev libdb4.8++-dev -y
sudo apt-get install libzmq5 python-virtualenv -y

## Bắt đầu cài đặt ví
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> COMPILING WALLET...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>COMPILING WALLET...{$RESET} \n\n"
cd /tmp
git clone $GIT_REPO_URL
cd $REPO_DIR
sudo chmod 755 autogen.sh
sudo ./autogen.sh
sudo ./configure
sudo chmod 755 share/genbuild.sh
sudo make
cd src/
sudo cp $DAEMON_NAME $LOCAL_BIN
sudo cp $CLI_NAME $LOCAL_BIN

#####################
## CÀI ĐẶT VÍ XONG ##
#####################

#ghi file config của Masternode 
cd $CONF_DIR
printf "rpcuser=%s
rpcpassword=%s
rpcallowip=127.0.0.1" $RPC_USER $RPC_PASS > $CONF_FILE

# Khởi động ví
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> START DAEMON...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>START DAEMON...{$RESET} \n\n"
$DAEMON_PATH -daemon &
# Tạm dừng 5s để ví khởi chạy xong
sleep 5
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> GET/SET MASTERNODE PRIVATE KEY...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>GET/SET MASTERNODE PRIVATE KEY...{$RESET} \n\n"
# Lấy mansternode key
MN_PRIV_KEY=$($CLI_PATH masternode genkey)
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> STOP DAEMON...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>STOP DAEMON...{$RESET} \n\n"
# Dừng ví
$CLI_PATH stop

# Tạm dừng 5s để ví dừng hẳn
sleep 5

export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> WRITING WALLET CONFIG FILE...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>WRITING WALLET CONFIG FILE...{$RESET} \n\n"
# Ghi tất cả thông số vào file cấu hình masternode 
printf "rpcallowip=127.0.0.1
rpcuser=%s
rpcpassword=%s
rpcport=%s
rpcthreads=8
port=%s
externalip=%s
masternodeprivkey=%s
server=1
maxconnections=256
logtimestamps=1
listen=1
discover=1
daemon=1
staking=0
masternode=1" $RPC_USER $RPC_PASS $RPC_PORT_NUM $PORT_NUM $EXTERNAL_IP $MN_PRIV_KEY >  $CONF_FILE

# khởi động ví 
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> START DAEMON...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "\n\n 		{$CYAN}==>START DAEMON...{$RESET} \n\n"
$DAEMON_PATH -daemon &

# đặt crontab chạy ví khi hệ thống khởi động
(sudo crontab -l 2>/dev/null; echo "@reboot $DAEMON_PATH -daemon") | crontab
 sleep 3
# cài đặt và cấu hình UFW
if [ $SETUP_FW = "y" ]
	then
	export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> INSTALLING AND CONFIGURING FIREWALL...\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
	printf "\n\n 			{$RED}==>INSTALLING AND CONFIGURING FIREWALL...{$RESET} \n\n"
	sudo apt-get install ufw
	sudo ufw default deny incoming
	sudo ufw default allow outgoing
	sudo ufw allow ssh
	sudo ufw allow $PORT_NUM/tcp
	sudo ufw allow $RPC_PORT_NUM/tcp
	sudo ufw --force enable
fi

clear
export PS1='\[\e]0;MASTERNODE AUTO-INSTALLER ==> COMPLETED! \a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
printf "{$YELLOW}"
printf "\n\n###############################\n"
printf "##         COMPLETED!         ##\n"
printf "###############################\n\n"
printf "\n\n"
printf "\n {$CYAN} Your Masternode private Key: {$RED}%s" $MN_PRIV_KEY
printf "\n {$CYAN} You may need to install SENTINEL \n"
printf "\n\n"
printf "\n -------------------------------------------- \n\n"
printf "\n {$PINK}*** IF YOU FIND THIS SCRIPT USEFUL, GIVE ME A GLASS OF BEER! *** \n"
printf "BTC address: 1AyZnMa7kkmUnYPUG4vPp5PBV6mnqXxx8T0x0bbcfa7306bcc6f2a895c8bef55ac31bcfb44b8a \n"
printf "ETH address: 0x0bbcfa7306bcc6f2a895c8bef55ac31bcfb44b8a \n"
printf "CITR address: 5kggppC958wETt8CWeN8bJqNzf23h5367c \n"
printf "THANKS!{$RESET} \n"